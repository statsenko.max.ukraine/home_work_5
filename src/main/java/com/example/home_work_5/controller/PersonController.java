package com.example.home_work_5.controller;

import com.example.home_work_5.modal.Person;
import com.example.home_work_5.service.PersonService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/createPerson")
    public Person createPerson(@RequestBody Person person) {
        return personService.createPerson(person);
    }

    @GetMapping("/getAllPerson")
    public Set<Person> getAllPerson() {
        return personService.getAllPerson();
    }

    @GetMapping("/getPerson{id}")
    public Person getPerson(@PathVariable("id") @NonNull Integer id) {
        return personService.getPerson(id);
    }

    @PostMapping("/updatePerson")
    public Person updatePerson(@RequestBody Person person) {
        return personService.updatePerson(person);
    }

    @DeleteMapping("/deletePerson{id}")
    public void deletePerson(@PathVariable("id") @NonNull Integer id) {
        personService.deletePerson(id);
    }
}
