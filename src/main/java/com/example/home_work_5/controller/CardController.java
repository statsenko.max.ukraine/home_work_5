package com.example.home_work_5.controller;

import com.example.home_work_5.modal.Card;
import com.example.home_work_5.service.CardService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CardController {

    private final CardService cardService;

    @Autowired
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    @GetMapping("/person{personId}/card/addCardProduct{productId}")
    public Card addCardProduct(@PathVariable("personId") @NonNull Integer personId, @PathVariable("productId") @NonNull Integer productId) {
        return cardService.addCardProduct(personId, productId);
    }

    @GetMapping("/person{id}/card/getAllCardProduct")
    public Card getAllCardProduct(@PathVariable("id") @NonNull Integer id) {
        return cardService.getAllCardProduct(id);
    }

    @DeleteMapping("/person{personId}/card/deleteCardProduct{productId}")
    public void deleteCardProduct(@PathVariable("personId") @NonNull Integer personId, @PathVariable("productId") @NonNull Integer productId) {
        cardService.deleteCardProduct(personId, productId);
    }

    @GetMapping("/person{id}/card/getCardProductPrice")
    public Double getCardProductPrice(@PathVariable("id") @NonNull Integer id) {
        return cardService.getCardProductPrice(id);
    }
}
