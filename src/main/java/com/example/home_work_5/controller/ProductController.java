package com.example.home_work_5.controller;

import com.example.home_work_5.modal.Product;
import com.example.home_work_5.service.ProductService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/createProduct")
    public Product createPerson(@RequestBody Product product) {
        return productService.createProduct(product);
    }

    @GetMapping("/getAllProduct")
    public Set<Product> getAllProduct() {
        return productService.getAllProduct();
    }

    @GetMapping("/getProduct{id}")
    public Product getPerson(@PathVariable("id") @NonNull Integer id) {
        return productService.getProduct(id);
    }

    @PostMapping("/updateProduct")
    public Product updatePerson(@RequestBody Product person) {
        return productService.updateProduct(person);
    }

    @DeleteMapping("/deleteProduct{id}")
    public void deletePerson(@PathVariable("id") @NonNull Integer id) {
        productService.deleteProduct(id);
    }
}
