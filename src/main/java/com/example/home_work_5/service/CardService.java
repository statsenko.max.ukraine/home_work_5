package com.example.home_work_5.service;

import com.example.home_work_5.modal.Card;

public interface CardService {

    Card addCardProduct(Integer personId, Integer productId);

    Card getAllCardProduct(Integer id);

    Card deleteCardProduct(Integer personId, Integer productId);

    Double getCardProductPrice(Integer personId);
}
