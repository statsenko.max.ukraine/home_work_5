package com.example.home_work_5.service;

import com.example.home_work_5.modal.Product;

import java.util.Set;

public interface ProductService {

    Product createProduct(Product product);

    Product updateProduct(Product product);

    Product getProduct(Integer id);

    Set<Product> getAllProduct();

    void deleteProduct(Integer id);
}
