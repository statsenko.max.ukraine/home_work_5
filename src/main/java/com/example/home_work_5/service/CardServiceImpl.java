package com.example.home_work_5.service;

import com.example.home_work_5.modal.Card;
import com.example.home_work_5.modal.Person;
import com.example.home_work_5.storage.PersonStorage;
import com.example.home_work_5.storage.ProductStorage;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CardServiceImpl implements CardService {
    @Override
    public Card addCardProduct(@NonNull Integer personId, @NonNull Integer productId) {
        Person person = Objects.requireNonNull(PersonStorage.personStorageSet
                .stream()
                .filter(v -> v.getId().equals(personId))
                .findAny()
                .orElse(null));

        ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getId().equals(productId))
                .findAny()
                .ifPresent(product -> person.getCard()
                        .getProductList()
                        .add(product));

        return person.getCard();
    }

    @Override
    public Card getAllCardProduct(@NonNull Integer personId) {
        return Objects.requireNonNull(PersonStorage.personStorageSet
                        .stream()
                        .filter(v -> v.getId().equals(personId))
                        .findAny()
                        .orElse(null))
                .getCard();
    }

    @Override
    public Card deleteCardProduct(@NonNull Integer personId, @NonNull Integer productId) {
        Card card = Objects.requireNonNull(PersonStorage.personStorageSet
                        .stream()
                        .filter(v -> v.getId().equals(personId))
                        .findAny()
                        .orElse(null))
                .getCard();

        card.getProductList().removeIf(v -> v.getId().equals(productId));

        return card;
    }

    @Override
    public Double getCardProductPrice(@NonNull Integer personId) {
        return Objects.requireNonNull(PersonStorage.personStorageSet
                        .stream()
                        .filter(v -> v.getId().equals(personId))
                        .findAny()
                        .orElse(null))
                .getCard()
                .getSum();
    }
}
