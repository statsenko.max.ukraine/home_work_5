package com.example.home_work_5.service;

import com.example.home_work_5.modal.Person;

import java.util.Set;

public interface PersonService {

    Person createPerson(Person person);

    Person updatePerson(Person person);

    Person getPerson(Integer id);

    Set<Person> getAllPerson();

    void deletePerson(Integer id);
}
