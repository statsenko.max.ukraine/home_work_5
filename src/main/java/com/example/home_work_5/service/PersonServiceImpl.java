package com.example.home_work_5.service;

import com.example.home_work_5.modal.Card;
import com.example.home_work_5.modal.Person;
import com.example.home_work_5.storage.PersonStorage;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PersonServiceImpl implements PersonService {
    private static Integer PEOPLE_COUNT = 1;

    static {
        PersonStorage.personStorageSet.add(new Person(1, "First", "Last", 11, "+380977777777", "test@tes.com"));
    }

    @Override
    public Person createPerson(Person person) {
        person.setId(++PEOPLE_COUNT);
        PersonStorage.personStorageSet.add(person);
        return person;
    }

    @Override
    public Person updatePerson(Person updatedPerson) {
        Person personToBeUpdated = getPerson(updatedPerson.getId());

        deletePerson(personToBeUpdated.getId());
        PersonStorage.personStorageSet.add(updatedPerson);
        return updatedPerson;
    }

    @Override
    public Person getPerson(Integer id) {
        return PersonStorage.personStorageSet
                .stream()
                .filter(v -> v.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    @Override
    public Set<Person> getAllPerson() {
        return PersonStorage.personStorageSet;
    }

    @Override
    public void deletePerson(Integer id) {
        PersonStorage.personStorageSet.removeIf(v -> v.getId().equals(id));
    }
}
