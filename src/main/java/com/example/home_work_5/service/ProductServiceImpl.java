package com.example.home_work_5.service;

import com.example.home_work_5.modal.Product;
import com.example.home_work_5.storage.ProductStorage;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ProductServiceImpl implements ProductService {
    private static Integer PRODUCT_COUNT = 3;

    static {
        ProductStorage.productStorageSet.add(new Product(1, "Bread", 1.21));
        ProductStorage.productStorageSet.add(new Product(2, "Cookies", 3.0));
        ProductStorage.productStorageSet.add(new Product(3, "Milk", 1.5));
    }

    @Override
    public Product createProduct(Product product) {
        product.setId(++PRODUCT_COUNT);
        ProductStorage.productStorageSet.add(product);
        return product;
    }

    @Override
    public Product updateProduct(Product updatedProduct) {
        Product productToBeUpdated = getProduct(updatedProduct.getId());

        deleteProduct(productToBeUpdated.getId());
        ProductStorage.productStorageSet.add(updatedProduct);
        return updatedProduct;
    }

    @Override
    public Product getProduct(Integer id) {
        return ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    @Override
    public Set<Product> getAllProduct() {
        return ProductStorage.productStorageSet;
    }

    @Override
    public void deleteProduct(Integer id) {
        ProductStorage.productStorageSet
                .removeIf(v -> v.getId().equals(id));
    }
}
