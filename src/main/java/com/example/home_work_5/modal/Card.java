package com.example.home_work_5.modal;

import lombok.EqualsAndHashCode;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@EqualsAndHashCode
public class Card {
    List<Product> productList;
    Double sum;

    public Card() {
        this.productList = new ArrayList<>();
        this.sum = 0.0;
    }

    public List<Product> getProductList() {
        this.sum = calculateSum();
        return this.productList;
    }

    public Double getSum() {
        this.sum = calculateSum();
        return this.sum;
    }

    private Double calculateSum() {
        Double sumProduct = 0.0;
        for (Product product : productList) {
            sumProduct += product.getPrice();
        }
        return sumProduct;
    }
}
