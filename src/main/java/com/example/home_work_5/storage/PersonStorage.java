package com.example.home_work_5.storage;

import com.example.home_work_5.modal.Person;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class PersonStorage {
    public static Set<Person> personStorageSet = new HashSet<>(new TreeSet<>(Comparator.comparing(Person::getId)));
}
