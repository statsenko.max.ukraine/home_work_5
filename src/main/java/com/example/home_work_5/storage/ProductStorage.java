package com.example.home_work_5.storage;

import com.example.home_work_5.modal.Product;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class ProductStorage {
    public static Set<Product> productStorageSet = new HashSet<>(new TreeSet<>(Comparator.comparing(Product::getId)));
}
