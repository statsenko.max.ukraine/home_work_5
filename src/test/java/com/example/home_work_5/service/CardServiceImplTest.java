package com.example.home_work_5.service;

import com.example.home_work_5.modal.Card;
import com.example.home_work_5.modal.Product;
import com.example.home_work_5.storage.PersonStorage;
import com.example.home_work_5.storage.ProductStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@SpringBootTest
class CardServiceImplTest {
    @Test
    void addCardProduct() {
        Card actualCard = new CardServiceImpl().addCardProduct(1, 2);

        Product product = ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getId().equals(2))
                .findAny()
                .orElse(null);
        List<Product> productList = new ArrayList<>();
        productList.add(product);

        Card expectedCard = new Card();
        expectedCard.setProductList(productList);

        Assertions.assertEquals(expectedCard, actualCard, "Card objects don't match");

        new CardServiceImpl().deleteCardProduct(1, 2);
    }

    @Test
    void getAllCardProduct() {
        Card actualCard = new CardServiceImpl().getAllCardProduct(1);

        Card expectedCard = Objects.requireNonNull(PersonStorage.personStorageSet
                        .stream()
                        .filter(v -> v.getId().equals(1))
                        .findAny()
                        .orElse(null))
                .getCard();

        Assertions.assertEquals(expectedCard, actualCard, "Card objects don't match");
    }

    @Test
    void deleteCardProduct() {
        Card actualCard = new CardServiceImpl().deleteCardProduct(1, 2);

        Card expectedCard = new Card();
        actualCard.setProductList(new ArrayList<>());

        Assertions.assertEquals(expectedCard, actualCard, "Card objects don't match");
    }

    @Test
    void getCardProductPrice() {
        new CardServiceImpl().addCardProduct(1, 2);
        Double actualPrice = new CardServiceImpl().getCardProductPrice(1);

        Double expectedPrice = Objects.requireNonNull(ProductStorage.productStorageSet
                        .stream()
                        .filter(v -> v.getId().equals(2))
                        .findFirst()
                        .orElse(null))
                .getPrice();

        Assertions.assertEquals(expectedPrice, actualPrice, "Price don't match");
    }
}