package com.example.home_work_5.service;

import com.example.home_work_5.modal.Person;
import com.example.home_work_5.storage.PersonStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
class PersonServiceImplTest {
    @Test
    void createPerson() {
        Person actualPerson = new PersonServiceImpl().createPerson(new Person(null, "CreatePersonTest", "Test", 12, "+380977777777", "testing@tes.com"));
        Person expectedPerson = PersonStorage.personStorageSet
                .stream()
                .filter(v -> v.getFirstName().equals("CreatePersonTest"))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");

        new PersonServiceImpl().deletePerson(2);
    }

    @Test
    void updatePerson() {
        Person actualPerson = new PersonServiceImpl().updatePerson(new Person(1, "UpdatePersonTest", "Test", 15, "+380977777777", "testing@tes.com"));
        Person expectedPerson = PersonStorage.personStorageSet
                .stream()
                .filter(v -> v.getId().equals(1))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");
    }

    @Test
    void getPerson() {
        Person actualPerson = new PersonServiceImpl().getPerson(1);
        Person expectedPerson = PersonStorage.personStorageSet
                .stream()
                .filter(v -> v.getId().equals(1))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualPerson, "Person objects don't match");
    }

    @Test
    void getAllPerson() {
        Set<Person> actualPerson = new PersonServiceImpl().getAllPerson();
        Set<Person> expectedPerson = PersonStorage.personStorageSet;

        Assertions.assertEquals(expectedPerson, actualPerson, "Set person doesn't match");
    }

    @Test
    void deletePerson() {
        Set<Person> expectedPerson = PersonStorage.personStorageSet;
        new PersonServiceImpl().createPerson(new Person(null, "CreatePersonTest", "Test", 12, "+380977777777", "testing@tes.com"));
        new PersonServiceImpl().deletePerson(2);
        Set<Person> actualPerson = PersonStorage.personStorageSet;


        Assertions.assertEquals(expectedPerson, actualPerson, "Set person doesn't match");
    }
}