package com.example.home_work_5.service;

import com.example.home_work_5.modal.Product;
import com.example.home_work_5.storage.ProductStorage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Set;

@SpringBootTest
class ProductServiceImplTest {
    @Test
    void createProduct() {
        Product actualProduct = new ProductServiceImpl().createProduct(new Product(null, "CreateProductTest", 1.11));
        Product expectedPerson = ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getName().equals("CreateProductTest"))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualProduct, "Product objects don't match");

        new ProductServiceImpl().deleteProduct(4);
    }

    @Test
    void updateProduct() {
        Product actualProduct = new ProductServiceImpl().updateProduct(new Product(1, "UpdateProductTest", 1.11));
        Product expectedPerson = ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getId().equals(1))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualProduct, "Product objects don't match");
    }

    @Test
    void getProduct() {
        Product actualProduct = new ProductServiceImpl().getProduct(2);
        Product expectedPerson = ProductStorage.productStorageSet
                .stream()
                .filter(v -> v.getId().equals(2))
                .findAny()
                .orElse(null);

        Assertions.assertEquals(expectedPerson, actualProduct, "Product objects don't match");
    }

    @Test
    void getAllProduct() {
        Set<Product> actualProduct = new ProductServiceImpl().getAllProduct();
        Set<Product> expectedPerson = ProductStorage.productStorageSet;

        Assertions.assertEquals(expectedPerson, actualProduct, "Set product doesn't match");
    }

    @Test
    void deleteProduct() {
        Set<Product> expectedProduct = ProductStorage.productStorageSet;
        new ProductServiceImpl().createProduct(new Product(null, "CreateProductTest", 1.11));
        new ProductServiceImpl().deleteProduct(4);
        Set<Product> actualProduct = ProductStorage.productStorageSet;


        Assertions.assertEquals(expectedProduct, actualProduct, "Set product doesn't match");
    }
}